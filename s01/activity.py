#Creating Lists

numbers = [1, 2, 3, 4, 5]
print("list of meals:", numbers)
meals = ["spam", "Spam", "SPAM!"]
print("list of meals:", meals)

messages = []
messages.extend(["Hi!"]*4)
print("Result of repeat operator:", messages)

print("List literals and operations using 'numbers' list:")
print("Length of the list:",len(numbers))
print("The last item was removed from list:", numbers.pop())
print("The list in reversed order:", list(reversed(numbers)))


print("List literals and operations using 'meals' list:")
print("using indexing operation:", meals[2])
print("Using negative indexing operation:", meals[-2])
meals[1] = "eggs"
print("Modified list:", meals)
meals.append("bacon")
print("Added a new item in the list:", meals)
meals.sort()
print("Modified list after sort method:", meals )

#Creating a dictionary
recipe = {
	"eggs": 3
}

print("Created recipe dictionary:")
print(recipe)

recipe["sapm"] = 2
recipe["ham"] = 1
recipe['brunch'] = "bacon"
print("Modified recipe dictionary:")
print(recipe)

recipe["ham"] = ["grill", "bake", "fry"]
print("Updated the 'ham' key value:")
print(recipe)


recipe.pop("eggs")
print("Modified recipe after deleting 'eggs' key")
print(recipe)

bob = {
	"name": {
		"first": "Bob",
		"last": "Smith"
	},
	"age": 42,
	"job": ["software", "writing"],
	"pay": (40000, 50000)
}
print("Given 'bob' dictionary:")
print(bob)

print("Accessing the value of 'name' key:", bob['name'])
print("Accessing the value of 'last' key:", bob["name"]['last'])
print("Accessing the second value of 'pay' key:", bob['pay'][1])

numeric = ('twelve', 3.0, [11, 22, 33])
print("Given 'numeric tuple:'")
print(numeric)

print("Accessing a tuple item:", numeric[1])
print("Accessing a tuple item:", numeric[2][1])