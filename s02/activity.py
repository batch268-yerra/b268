class Node:
    def __init__(self, data):
        self.data = data
        self.next = None

class ColorLinkedList:
    def __init__(self):
        self.head = None

    def append(self, color):
        new_node = Node(color)
        if not self.head:
            self.head = new_node
            return
        current = self.head
        while current.next:
            current = current.next
        current.next = new_node

    def prepend(self, color):
        new_node = Node(color)
        new_node.next = self.head
        self.head = new_node

    def delete(self, color):
        current = self.head
        if current and current.data == color:
            self.head = current.next
            current = None
            return
        prev = None
        while current and current.data != color:
            prev = current
            current = current.next
        if current is None:
            return
        prev.next = current.next
        current = None

    def search(self, color):
        current = self.head
        while current:
            if current.data == color:
                return "Yes"
            current = current.next
        return "No"

    def count(self):
        count = 0
        current = self.head
        while current:
            count += 1
            current = current.next
        return count

    def print_list(self):
        current = self.head
        while current:
            print(current.data)
            current = current.next
        print("None")

# Instantiate ColorLinkedList
clist = ColorLinkedList()

# Insert colors: Brown, Green, Grey, Black
clist.append("Brown")
clist.append("Green")
clist.append("Blue")
clist.append("Grey")
clist.append("Black")

# Print the nodes of the linked list
print("Original nodes of the linked list:")
clist.print_list()

# Insert the color Purple at the head of the linked list
clist.prepend("Purple")
print('Insert the color "Purple" at the front of the linked list:')
clist.print_list()

# Remove the color Black
clist.delete("Black")
print('Delete the color "Black" from the linked list:')
clist.print_list()

# Check if Grey and Black are in the list
print('Does the color "Grey" exist in the linked list?')
print(clist.search("Grey"))
print('Does the color "Black" exist inthe linked list?')
print(clist.search("Black"))

# Count the total number of nodes in the list
print("Total number of nodes:")
print(clist.count())
